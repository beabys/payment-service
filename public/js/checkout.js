
$(document).ready(function() {

    $('#credit_number').simpleMask( { 'mask': ['####-####-####-####', '####-####-####-###','#####-####-####-####']} );
    $('#credit_expiration').simpleMask( { 'mask': '##/##'} );

});