<?php

return [
    'braintree' => [
        'environment' => env('BRAINTREE_ENVIRONMENT', 'sandbox'),
        'merchant_id' => env('BRAINTREE_MERCHANT_ID',  ''),
        'public_key' => env('BRAINTREE_PUBLIC_KEY',  ''),
        'private_key' => env('BRAINTREE_PRIVATE_KEY', ''),
        'currency_iso' => env('BRAINTREE_CURRENCY_ISO', 'USD')
    ],
    'paypal' => [
        'environment' => env('PAYPAL_ENVIRONMENT', 'sandbox'),
        'client_id' => env('PAYPAL_CLIENT_ID', ''),
        'secret' => env('PAYPAL_SECRET', ''),
    ]
];
