### Requirements

Create a payment gateway library, that could handle payments with
* PayPal REST API
* Braintree payments
 
### Create Payment

Create a Make Payment Form to make the payment. For should have this fields

* Order Section
* Customer Name
* Customer Phone Number
* Currency (HKD, USD, AUD, EUR, JPY, CNY)
* Price

#### Payment Section

* Credit card holder name
* Credit card number
* Credit card expiration
* Credit card CCV
* Submit Button

After submitting the payment form, use a different gateway based on these rules:

* If credit card is AMEX, then use PayPal;
* If currency is USD, EUR, or AUD, then use PayPal. Otherwise use Braintree;
* If currency is not USD and the credit card is AMEX, return error message that AMEX is possible to use only for USD;

Use appropriate form validations.

Show Lightbox with success or error message after payment. If success, give the payment reference code to the user for record checking.

Save order data + response from payment gateway to database table.

### Check Payment

Create a Payment Checking Form to check the payment record by Customer name, and payment reference code. If success, show Customer Name, Customer Phone Number, Currency and Price. If failed, show Lightbox of Record No Found Message

The record should get from memory caching instead of database

### Test Submission

Create a public repository on any Git repository provider and push the solution there. Send us the link to the repository

### Hints for Bonus

* Consider how to add additional payment gateways;
* Consider how to guarantee payment record are found in your database and Paypal;
* Consider cache may expired when check order record;
* Consider the data encryption for the payment record query
* Consider how to handle security for saving credit cards.

### Specification

* Create your own sandbox accounts for payment gateway
* Use any programming language and MVC framework that is your favorites.
* Don’t bother with any fancy UI, just use UI Framework such as Bootstrap
* Use only PayPal and Braintree official libraries. You can use React JS/ Angular JS  / jQuery for form validation. Do not use any 3rd party payment libraries.
* Use only redis for memory caching and any database engine.
* No need to store the credit card information unless you really want to try.
* The code needs to work after we clone it and try it (no bugs) and should process the payments.
* Please prepare the document for the data structure and detail setup instruction or/and publicly accessible URL.
