<?php

namespace ExchangeRate;

/**
 * Class Success
 * @package ExchangeRate
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Success
{
    protected $result;

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param $result
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

}