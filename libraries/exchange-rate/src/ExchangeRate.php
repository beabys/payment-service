<?php

namespace ExchangeRate;

use Exception;
/**
 * Class PaymentStrategy
 * @package PaymentEngine
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class FixerIo
{
    /**
     * @param $base
     * @return Error|Success
     */
    public function build($base)
    {
        $curl = curl_init();
        $error = new Error();
        try {
            $curlOp = [
                CURLOPT_URL => "http://api.fixer.io/latest?base=" . $base,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "cache-control: no-cache",
                    "content-type: application/json"
                ],
            ];
            curl_setopt_array($curl, $curlOp);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return $error->setMessage("cURL Error #:" . $err);
            }
            $response = json_decode($response, true);
            if (isset ($response['rates'])) {
                $success = new Success();
                return $success->setResult($response['rates']);
            }
            return $error->setMessage('Error: No data in response');
        } catch (Exception $e) {
            return $error->setMessage('Error: ' . $e->getMessage());
        }
    }
}