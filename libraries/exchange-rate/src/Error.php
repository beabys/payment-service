<?php

namespace ExchangeRate;

/**
 * Class Error
 * @package ExchangeRate
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Error
{
    protected $message;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

}