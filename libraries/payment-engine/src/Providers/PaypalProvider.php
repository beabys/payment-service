<?php

namespace PaymentEngine\Providers;

use PaymentEngine\Messages\Error;
use PaymentEngine\Messages\Success;
use PayPal\Api\Amount;
use PayPal\Api\CreditCardToken;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentCard;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\CreditCard;

use DateTime;
use Exception;

/**
 * Class PaypalProvider
 * @package PaymentEngine\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaypalProvider extends AbstractProvider
{

    const PAYER = 'credit_card';
    const TEST_ITEM = 'test item';

    protected $name = 'paypal';
    protected $apiContext;
    protected $card;
    protected $fundingInstrument;
    protected $items;
    protected $payer;
    protected $itemList;
    protected $amount;
    protected $transaction;
    protected $payment;
    protected $details;
    protected $cardId = false;
    protected $payerId;
    protected $saveCard;

    /**
     * PaypalProvider constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $params['client_id'],
                $params['secret']
            )
        );
        $this->apiContext->setConfig(
            array(
                'mode' => $params['environment'],
                'log.LogEnabled' => 'sandbox' === $params['environment'] ?:false,
                'log.FileName' => '../PayPal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => 'sandbox' === $params['environment'] ?:false,
            )
        );
        $this->fundingInstrument = new FundingInstrument();
        $this->payer = new Payer();
        $this->itemList = new ItemList();
        $this->details = new Details();
        $this->amount = new Amount();
        $this->transaction = new Transaction();
        $this->payment = new Payment();
    }

    /**
     * @param $message
     * @param null $customerId
     * @param bool $saveCard
     * @return Success|Error
     */
    public function charge($message, $customerId = null, $saveCard = true)
    {
        $this->setObjects($message, $customerId, $saveCard);
        $payment = $this->getPayment()
            ->setIntent("sale")
            ->setPayer($this->getPayer())
            ->setTransactions([$this->getTransaction()])
        ;
        try {
            $payment->create($this->apiContext);
            $transaction = $payment->get($payment->getId(), $this->apiContext)->toArray();
            $state = isset($transaction['state']) ? $transaction['state'] : 'invalid';
            if ($state === 'approved') {
                $success = new Success();
                return $success
                    ->setTransactionId($transaction['id'])
                    ->setCustomerId($this->getCardId())
                    ->setPaymentResult($transaction)
                    ;
            }
            $error =  new Error('Transaction state: ' . $transaction['state']);
            return $error
                ->setPaymentResult($transaction)
                ->setCustomerId($this->getCardId())
                ;
        } catch (Exception $ex) {
            return new Error($ex->getMessage());
        }
    }

    /**
     * @param $message
     */
    protected function setCard($message)
    {
        $card = $this->getSaveCard() ? new CreditCard() : new PaymentCard();
        list($expMont, $year) =  explode('/', $message['credit_expiration']);
        $dates = DateTime::createFromFormat('y', $year);
        $expYear = $dates->format('Y');
        $card
            ->setType(strtolower($message['card_type']))
            ->setNumber(implode('', explode('-', $message['credit_number'])))
            ->setExpireMonth($expMont)
            ->setExpireYear($expYear)
            ->setCvv2($message['cvv'])
            ->setFirstName($message['firstname'])
            ->setLastName($message['lastname'])
        ;
        if ($this->getSaveCard()) {
            $card->setMerchantId($message['email']);
            $card->setExternalCardId($message['email'] . "-" . uniqid());
            try {
                $card->create($this->apiContext);
                //Card information needed to process payment
                $this->setCardId($card->getId());
                $this->setPayerId($card->getPayerId());
                return;
            } catch (Exception $ex) {
                die($ex);
            }
        }
        $card->setBillingCountry("HK");
        return;
    }

    /**
     * @return mixed
     */
    protected function setFundingInstruments()
    {
        if ($this->getSaveCard()) {
            $creditCardToken = new CreditCardToken();
            $creditCardToken->setCreditCardId($this->getCardId());
            return $this->getFundingInstrument()
                    ->setCreditCardToken($creditCardToken)
                ;
        }
        return $this->getFundingInstrument()
            ->setPaymentCard($this->getCard())
        ;
    }

    /**
     * @param $fundingInstruments
     * @return mixed
     */
    protected function setPayer($fundingInstruments)
    {
        return $this->getPayer()
            ->setPaymentMethod('credit_card')
            ->setFundingInstruments([$fundingInstruments])
        ;
    }

    /**
     * @param $message
     * @param array $item
     * @return Item
     */
    protected function setItem($message, $item = [])
    {
        $itemElement = new Item();
        $itemElement
            ->setName(isset($item['name']) ? $item['name'] : self::TEST_ITEM)
            ->setDescription(isset($item['name']) ? $item['name'] : self::TEST_ITEM)
            ->setCurrency($message['currency'])
            ->setQuantity(isset($message['quantity']) ? $message['quantity'] : 1)
            ->setTax(isset($message['tax']) ? $message['tax'] : 0)
            ->setPrice(isset($message['price']) ? $message['price'] : 1)
        ;
        return $itemElement;
    }

    /**
     * @param $items
     * @return mixed
     */
    protected function setItemList($items)
    {
        return $this->getItemList()
            ->setItems($items)
        ;
    }

    /**
     * @param $message
     * @return mixed
     */
    protected function setAmount($message)
    {
        return $this->getAmount()
            ->setCurrency($message['currency'])
            ->setTotal(isset($message['price']) ? $message['price'] : 1)
        ;
    }

    /**
     * @param $amount
     * @param $itemList
     * @param $message
     */
    protected function setTransaction($amount, $itemList, $message)
    {
        $this->getTransaction()
            ->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription(isset($message['payment_description']) ?
                $message['payment_description'] :
                "Payment description"
            )
            ->setInvoiceNumber(isset($message['invoiceid']) ? $message['invoiceid'] : uniqid())
        ;
    }

    /**
     * @param $message
     * @param $customerId
     * @param bool $saveCard
     */
    protected function setObjects($message, $customerId, $saveCard = true)
    {
        $customerId = is_null($customerId) ? false : $customerId;
        $this->setCardId($customerId);
        $this->setSaveCard($saveCard);
        if (!$this->getCardId()) {
            $this->setCard($message);
        }
        $fi = $this->setFundingInstruments();
        $payer = $this->setPayer($fi);
        $item1 = $this->setItem($message);
        $items = [
            $item1,
        ];
        $itemList = $this->setItemList($items);
        $amount = $this->setAmount($message);
        $this->setTransaction($amount, $itemList, $message);

    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return mixed
     */
    public function getFundingInstrument()
    {
        return $this->fundingInstrument;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @return mixed
     */
    public function getItemList()
    {
        return $this->itemList;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return mixed
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @param $cardId
     * @return $this
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;

        return$this;
    }

    /**
     * @return mixed
     */
    public function getPayerId()
    {
        return $this->payerId;
    }

    /**
     * @param $payerId
     * @return $this
     */
    public function setPayerId($payerId)
    {
        $this->payerId = $payerId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSaveCard()
    {
        return $this->saveCard;
    }

    /**
     * @param $saveCard
     * @return $this
     */
    public function setSaveCard($saveCard)
    {
        $this->saveCard = $saveCard;

        return $this;
    }

}
