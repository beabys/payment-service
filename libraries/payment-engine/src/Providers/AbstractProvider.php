<?php

namespace PaymentEngine\Providers;

/**
 * Class AbstractProvider
 * @package PaymentEngine\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
abstract class AbstractProvider implements ProviderInterface
{

    const SUCCESS_PAYMENT = 'success';

    const PENDING_PAYMENT = 'pending';

    const PENDING_CANCEL = 'cancel';

    /**
     * @var paymentMethod name
     */
    protected $name = null;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function charge($message)
    {
        // TODO: Implement charge() method.
    }

}
