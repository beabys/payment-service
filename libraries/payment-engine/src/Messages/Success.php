<?php

namespace PaymentEngine\Messages;

/**
 * Class Success
 * @package PaymentEngine\Messages
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Success
{
    protected $customerId = null;
    protected $paymentResult = null;
    protected $transactionId = null;

    /**
     * @return null
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return null
     */
    public function getPaymentResult()
    {
        return $this->paymentResult;
    }

    /**
     * @param $paymentResult
     * @return $this
     */
    public function setPaymentResult($paymentResult)
    {
        $this->paymentResult = $paymentResult;

        return $this;
    }

    /**
     * @return null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param $transactionId
     * @return $this
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

}