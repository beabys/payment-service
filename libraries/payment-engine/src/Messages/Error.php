<?php

namespace PaymentEngine\Messages;

/**
 * Class Error
 * @package PaymentEngine\Messages
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Error
{
    protected $error;

    protected $customerId = null;

    protected $paymentResult = null;

    /**
     * Error constructor.
     * @param $message
     */
    public function __construct($message)
    {
        $this->setError($message);
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setError($message)
    {
        $this->error = $message;

        return $this;
    }

    /**
     * @return null
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return null
     */
    public function getPaymentResult()
    {
        return $this->paymentResult;
    }

    /**
     * @param $paymentResult
     * @return $this
     */
    public function setPaymentResult($paymentResult)
    {
        $this->paymentResult = $paymentResult;

        return $this;
    }

}