<?php

namespace PaymentEngine;

use PaymentEngine\Providers\BraintreeProvider;
use PaymentEngine\Providers\PaypalProvider;
use Exception;

/**
 * Class PaymentStrategy
 * @package PaymentEngine
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaymentStrategy
{

    const BRAINTREE = "braintree";
    const PAYPAL = 'paypal';

    /**
     * @param $paymentMethod
     * @param null $params
     * @return BraintreeProvider|PaypalProvider
     * @throws Exception
     */
    public function build($paymentMethod, $params = null)
    {
        switch ($paymentMethod) {
            case self::BRAINTREE :
                return new BraintreeProvider($params);
                break;
            case self::PAYPAL :
                return new PaypalProvider($params);
                break;
            default :
                throw new Exception(sprintf("payment Method %s not defined", $paymentMethod));
                break;
        }
    }

}
