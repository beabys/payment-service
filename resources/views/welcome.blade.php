@extends('../main-layout')

@section('content')
    <h1><a id="Payment_Service_0"></a>Payment Service</h1>
    <h2><a id="Description_of_the_project_2"></a>Description of the project</h2>
    <ul>
        <li><a href="./docs/definition.md">Definition</a>
        </li>
    </ul>
    <h2><a id="Prerequisites_6"></a>Prerequisites</h2>
    <ul>
        <li><a href="https://getcomposer.org/">Composer</a>
        </li>
        <li><a href="https://redis.io/">Redis</a>
        </li>
        <li><a href="https://www.mysql.com/">MySQL</a> or <a href="https://mariadb.org/">MariaDB</a></li>
    </ul>
    <h2><a id="Payment_Services_Providers_10"></a>Payment Services Providers</h2>
    <ul>
        <li><a href="https://www.paypal.com/">Paypal</a>
        </li>
        <li><a href="https://www.braintreepayments.com/%E2%80%8E">Braintree</a>
        </li>
    </ul>
    <h1><a id="Installing_16"></a>Installing</h1>
    <h3><a id="Set_configurations_17"></a>Set configurations</h3>
    <p>copy the .env.example to .env and fill the variables required for configuration</p>
    <pre><code>cp .env.example .env</code></pre>
    <h3><a id="Configuration_Variables_23"></a>Configuration Variables</h3>
    <h4><a id="Redis_25"></a>Redis</h4>
    <pre><code>REDIS_HOST=
REDIS_PASSWORD=
REDIS_PORT=</code></pre>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Parameter</th>
            <th>Default Values</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>REDIS_HOST</td>
            <td>127.0.0.1</td>
        </tr>
        <tr>
            <td>REDIS_PASSWORD</td>
            <td>null</td>
        </tr>
        <tr>
            <td>REDIS_PORT</td>
            <td>6379</td>
        </tr>
        </tbody>
    </table>
    <h4><a id="Paypal_38"></a>Paypal</h4>
    <pre><code>PAYPAL_ENVIRONMENT=
PAYPAL_CLIENT_ID=
PAYPAL_SECRET=
</code></pre>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Parameter</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>PAYPAL_ENVIRONMENT</td>
            <td>(default)sandbox or live</td>
        </tr>
        <tr>
            <td>PAYPAL_CLIENT_ID</td>
            <td>Provided by Paypal</td>
        </tr>
        <tr>
            <td>PAYPAL_SECRET</td>
            <td>Provided by Paypal</td>
        </tr>
        </tbody>
    </table>
    <h4><a id="Braintree_50"></a>Braintree</h4>
    <pre><code>    BRAINTREE_ENVIRONMENT=
    BRAINTREE_MERCHANT_ID=
    BRAINTREE_PUBLIC_KEY=
    BRAINTREE_PRIVATE_KEY=
</code></pre>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Parameter</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>BRAINTREE_ENVIRONMENT</td>
            <td>(default)sandbox or live</td>
        </tr>
        <tr>
            <td>BRAINTREE_MERCHANT_ID</td>
            <td>Provided by Braintree</td>
        </tr>
        <tr>
            <td>BRAINTREE_PUBLIC_KEY</td>
            <td>Provided by Braintree</td>
        </tr>
        <tr>
            <td>BRAINTREE_PRIVATE_KEY</td>
            <td>Provided by Braintree</td>
        </tr>
        </tbody>
    </table>
    <h3><a id="Install_Dependencies_65"></a>Install Dependencies</h3>
    <pre><code>composer install</code></pre>
    <h3><a id="Run_Migrations_69"></a>Run Migrations</h3>
    <pre><code>php artisan migrate</code></pre>
    <h3><a id="Start_Server_73"></a>Start Server</h3>
    <pre><code>php artisan serve</code></pre>
    <p>if you want to use a diferent port use the follow</p>
    <pre><code>php artisan serve --port[port]</code></pre>
    <h3><a id="Routes_82"></a>Routes</h3>
    <h5><a id="Start_Payment_84"></a>Start Payment</h5>
    <pre><code>http://localhost:[port]/payment</code></pre>
    <h5><a id="Check_Payment_88"></a>Check Payment</h5>
    <pre><code>http://localhost:[port]/checking</code></pre>
    <h3><a id="Braintree_test_cards_92"></a>Braintree test cards</h3>
    <ul>
        <li><a href="https://developers.braintreepayments.com/reference/general/testing/php">test cards</a>
        </li>
    </ul>
    <h3><a id="Paypal_Test_Cards_96"></a>Paypal Test Cards</h3>
    <ul>
        <li><a href="https://www.paypal-knowledge.com/infocenter/index?page=content&amp;widgetview=true&amp;id=FAQ1413">create test cards</a>
        </li>
    </ul>
    <h3><a id="Add_New_Payment_Methods_100"></a>Add New Payment Methods</h3>
    <p>To implement a new payment method, add the new instance of the payment method in the payment strategy</p>
    <pre><code>App\Components\Payments\PaymentStrategy</code></pre>
    <p>and set the configuration in the .env file and in config\payments.php</p>
    <h2><a id="Author_108"></a>Author</h2>
    <p>This Payment Service was created by Alfonso Rodriguez (<a href="http://twitter.com/beabys">@beabys</a>).</p>

@endsection