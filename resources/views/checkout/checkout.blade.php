@extends('../main-layout')

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/checkout.js') }}"></script>
@endsection

@section('content')

    <form method="POST" action="{{ url('/checkout') }}" autocomplete="off">

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('holder_name') ? 'has-error' : '' }}">
                    <label for="holder_name">Credit Card Holder Name:</label>
                    <input type="text" id="holder_name" name="holder_name" class="form-control" placeholder="Enter Credit Card Holder Name" value="{{ old('holder_name') }}">
                    <span class="text-danger">{{ $errors->first('holder_name') }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('credit_number') ? 'has-error' : '' }}">
                    <label for="credit_number">Credit Card Number:</label>
                    <input type="text" id="credit_number" name="credit_number" class="form-control" placeholder="Enter Credit Card Number" value="{{ old('credit_number') }}">
                    <span class="text-danger">{{ $errors->first('credit_number') }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('credit_expiration') ? 'has-error' : '' }}">
                    <label for="credit_expiration">Credit Card Expiration Date:</label>
                    <input type="text" id="credit_expiration" name="credit_expiration" class="form-control" placeholder="Enter Credit Card Expiration Date" value="{{ old('credit_expiration') }}">
                    <span class="text-danger">{{ $errors->first('credit_expiration') }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group {{ $errors->has('cvv') ? 'has-error' : '' }}">
                    <label for="cvv">Credit Card Number:</label>
                    <input type="password" id="cvv" name="cvv" class="form-control" placeholder="CVV" maxlength="4" value="{{ old('cvv') }}">
                    <span class="text-danger">{{ $errors->first('cvv') }}</span>
                </div>
            </div>
        </div>


        <div class="form-group">
            <button class="btn btn-success">Submit</button>
        </div>

    </form>
@endsection
