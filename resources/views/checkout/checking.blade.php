@extends('../main-layout')

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/checkout.js') }}"></script>
@endsection

@section('content')
    
    <form method="POST" action="{{ url('/checking') }}" autocomplete="off">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('firstName') ? 'has-error' : '' }}">
                    <label for="firstName">First Name:</label>
                    <input type="text" id="firstName" name="firstName" class="form-control" placeholder="Enter First Name" value="{{ old('firstName') }}">
                    <span class="text-danger">{{ $errors->first('firstName') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('transactionId') ? 'has-error' : '' }}">
                    <label for="transactionId">Transaction ID:</label>
                    <input type="text" id="transactionId" name="transactionId" class="form-control" placeholder="Enter Transaction ID" value="{{ old('transactionId') }}">
                    <span class="text-danger">{{ $errors->first('transactionId') }}</span>
                </div>
            </div>
        </div>


        <div class="form-group">
            <button class="btn btn-success">Submit</button>
        </div>
        <br />
        <br />
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                @endif
            @endforeach
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @if (isset($data) && !empty($data))
            <div class="col-md-6">
                <table class="table table-bordered table-striped" id="datatable-default">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                        <tbody>
                        @foreach($data as $key => $row)
                            <tr class="gradeX">
                                <td>{{ $key }}</td>
                                <td>{{ $row }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
            </div>
        @endif

    </form>
@endsection
