<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Facades\Redis;
use PaymentEngine\Messages\Success;

/**
 * Class RedisModel
 * @package App\Models
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class RedisModel
{
    protected $key = 'app';

    protected $fieldsToObfuscate = [
        'credit_number',
    ];

    protected $fieldsToDelete = [
        '_token',
        'cvv',
    ];
    /**
     * @param Success $paymentResponse
     * @param $parameters
     * @return array
     */
    public function saveId(Success $paymentResponse, $parameters)
    {
        $key = $paymentResponse->getTransactionId();
        $payment = [
            'firstName' => $parameters['firstname'],
            'lastName'  => $parameters['lastname'],
            'e-mail'    => $parameters['email'],
            'request'   => $this->obfuscate($parameters),
            'response'  => $paymentResponse->getPaymentResult(),
        ];
        $this->save($payment, $key);
        return $payment;
    }

    /**
     * @param $key
     * @return bool
     */
    public function delete($key)
    {
        return $this->del($key);
    }

    /**
     * @param $key
     * @param $parameters
     * @return bool
     */
    public function save($parameters, $key = null) {
        return $this->set($key, $parameters);
    }

    /**
     * @param $key
     * @return bool|mixed
     */
    public function getDataFromKey($key)
    {
        return $this->get($key);
    }

    /**
     * @param $key
     * @param array $parameters
     * @return array|mixed
     */
    protected function dataInArray($key, array $parameters)
    {
        return array_key_exists($key, $parameters) ? $parameters[$key] : [];
    }

    /**
     * @param array $parameters
     * @return mixed
     * @throws Exception
     */
    protected function getKey(array $parameters)
    {
        if (isset($parameters[$this->key])) {
            return $parameters[$this->key];
        }
        throw new Exception(sprintf('Key %s not exist in parameters', $this->key));
    }
    /**
     * @param $key
     * @param $values
     * @return bool
     * @throws Exception
     */
    private function set($key, $values)
    {
        try{
            $values = json_encode($values);
            Redis::set($key, $values);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $parameters
     * @return mixed
     */
    private function obfuscate($parameters)
    {
        foreach ($this->fieldsToObfuscate as $fields) {
            $parameters[$fields] = substr(str_replace(array('-', ' '), '', $parameters[$fields]), -4);
        }
        foreach ($this->fieldsToDelete as $delete)
        {
            unset($parameters[$delete]);
        }
        return $parameters;
    }

    /**
     * @param $key
     * @return bool|mixed
     * @throws Exception
     */
    private function get($key)
    {
        try{
            $result = Redis::get($key);
            if (!empty($result)) {
                return json_decode($result, true);
            }
            return [];
        } catch (Exception $e) {
            throw new Exception('Error in communication with Redis');
        }
    }

    /**
     * @param $key
     * @return bool
     */
    private function del($key)
    {
        try{
            $result = Redis::del($key);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}