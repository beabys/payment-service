<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payments
 * @package App\Models
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Payments extends Model
{
    protected $table = 'payments';

    /**
     * @param array $parameters
     * @return $this
     */
    public function savePayment(array $parameters)
    {
        $this->fk_customer = $parameters['request']['customer_id'];
        $this->provider = $parameters['request']['payment_method_name'];
        $this->request = serialize($parameters['request']);
        $this->response = serialize($parameters['response']);
        $this->save();
        return $this;
    }
}
