<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customers
 * @package App\Models
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Customers extends Model
{
    protected $table = 'customers';

    /**
     * @param array $parameters
     * @return mixed
     */
    public function saveCustomer(array $parameters)
    {
        $this->first_name = $parameters['firstname'];
        $this->last_name = $parameters['lastname'];
        $this->email = $parameters['email'];
        $this->phone = $parameters['phone'];
        $this->save();
        return $this;
    }
}
