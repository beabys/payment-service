<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CheckingFormRequest
 * @package App\Http\Requests
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class CheckingFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|min:5|max:35',
            'transactionId' => 'required|min:5|max:35',
        ];
    }
}
