<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PaymentFormRequest
 * @package App\Http\Requests
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaymentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|min:5|max:35',
            'lastname' => 'required|min:5|max:35',
            'email' => 'required|email',
            'phone' => 'required',
            'price' => ['required', 'numeric', 'regex:/^\d+([.]\d{0,2})?$/'],
            'currency' => 'required',
        ];
    }

}
