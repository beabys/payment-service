<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentFormRequest;
use App\Models\Customers;

/**
 * Class HomeController
 * @package App\Http\Controllers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class HomeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('./home.home');
    }

    /**
     * @param PaymentFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PaymentFormRequest $request)
    {
        $parameters = $request->all();
        //save in database Customer
        $data = $this->customerExist($parameters['email']);
        if (empty($data)) {
            $customer = new Customers();
            $customer->saveCustomer($parameters);
            $data = $customer->id;
        }
        $parameters['customer_id'] = $data;
        $request->session()->put('app', $parameters);
        return redirect()->route('checkout');
    }

    /**
     * @param $customer
     * @return mixed
     */
    protected function customerExist($customer)
    {
        $result = [];
        $query = Customers::where('email', $customer)->first();
        if (!empty($query)) {
            $result = $query->id;
        }
        return $result;
    }
}
