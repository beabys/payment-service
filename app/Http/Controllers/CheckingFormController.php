<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckingFormRequest;
use App\Traits\PaymentTrait;
use Illuminate\Support\Facades\Request;

class CheckingFormController extends Controller
{
    use PaymentTrait;

    const FAIL_MESSAGE = 'Record No Found';

    protected $returnParameters = [
        'Customer Name'=> 'firstname',
        'Customer Last Name'=> 'lastname',
        'Customer E-mail'=> 'email',
        'Customer Phone Number'=> 'phone',
        'Currency'=> 'currency',
        'Price'=> 'price',
        'Payment Method' => 'payment_method_name',
    ];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('./checkout.checking', request());
    }

    /**
     * @param CheckingFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getData(CheckingFormRequest $request)
    {
        $parameters = $request->all();
        $response = $this->getDataResponse($parameters);
        if (empty($response)) {
            $request->session()->flash('alert-danger', self::FAIL_MESSAGE);
        }
        return view('./checkout.checking', ['data' => $response]);
    }

    /**
     * @param array $request
     * @return array
     */
    private function getDataResponse(array $request)
    {
        $response = [];
        $model = $this->getModel();
        //getting data from cache
        $dataModel = $model->getDataFromKey($request['transactionId']);
        if (!empty($dataModel) && $dataModel['firstName'] == $request['firstName']) {
            $requestModel = $dataModel['request'];
            foreach ($this->returnParameters as $keyParameter => $parameter) {
                $response[$keyParameter] = $requestModel[$parameter];
            }
            $response['Transaction ID'] = $request['transactionId'];
            //Remove Data from Cache
            //Remove the comment only if the data is accessible only one time
            //$model->delete($request['transactionId']);
        }
        //implement here to get the request and response from DB if needed
        return $response;
    }
}
