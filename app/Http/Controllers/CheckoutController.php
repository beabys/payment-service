<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutFormRequest;
use App\Traits\CheckoutTrait;
use Illuminate\Http\Request;
use Exception;
use PaymentEngine\Messages\Error;

/**
 * Class CheckoutController
 * @package App\Http\Controllers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class CheckoutController extends Controller
{
    use CheckoutTrait ;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($request->session()->has('app')) {
            return view('./checkout.checkout');
        }
        return redirect()->route('payment');
    }

    /**
     * @param CheckoutFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function proceed(CheckoutFormRequest $request)
    {
        $parameters = array_merge(
            $request->session()->get('app'),
            $request->all()
        );
        try {
            $payment = $this->getPayment()->charge($parameters);
            if ($payment instanceof Error) {
                $request->session()->flash('alert-danger', $payment->getError());
                return redirect()->route('checkout');
            }
            $request->session()->put('success', true);
            $parametersMessage = [
                'transaction_id' => $payment->getTransactionId()
            ];
            return redirect()->route('success', $parametersMessage);
        } catch (Exception $e) {
            $request->session()->flash('alert-danger', $e->getMessage());
            return redirect()->route('checkout');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function success(Request $request)
    {
        if ($request->session()->has('success')) {
            $request->session()->flush();
            return view('./checkout.success', ['transactionId' => $request->get('transaction_id')]);
        }
        return redirect()->route('payment');
    }
}
