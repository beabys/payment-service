<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Components\Payments\PaymentStrategy as PaymentBuilder;

/**
 * Class PaymentServiceProvider
 * @package App\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaymentServiceProvider extends ServiceProvider
{

    const BUILDER  = 'paymentservice-builder';

    /**
     * Deferred load
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the builder
     */
    public function register()
    {
        $this
            ->registerBuilder()
        ;
    }

    /**
     * Register general builder
     * @return $this
     */
    protected function registerBuilder()
    {
        $this->app->bind(static::BUILDER, function () {
            $builder = (new PaymentBuilder());
            return $builder;
        });

        return $this;
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            static::BUILDER,
        ];
    }
}
