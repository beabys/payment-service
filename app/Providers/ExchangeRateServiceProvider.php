<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use ExchangeRate\FixerIo as ExchanRate;

/**
 * Class ExchangeRateServiceProvider
 * @package App\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class ExchangeRateServiceProvider extends ServiceProvider
{

    const BUILDER  = 'exchangerate-builder';

    /**
     * Deferred load
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the builder
     */
    public function register()
    {
        $this
            ->registerBuilder()
        ;
    }

    /**
     * Register general builder
     * @return $this
     */
    protected function registerBuilder()
    {
        $this->app->bind(static::BUILDER, function () {
            $builder = (new ExchanRate());
            return $builder;
        });

        return $this;
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            static::BUILDER,
        ];
    }
}
