<?php

namespace App\Traits;

use App\Models\RedisModel;

/**
 * Class PaymentTrait
 * @package App\Traits
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
trait PaymentTrait
{
    protected $redisModel;

    /**
     * @TODO This trait can be changed into a factory to handle another Cache service
     */

    /**
     * @return RedisModel
     */
    public function getModel()
    {
        if (!$this->redisModel instanceof RedisModel) {
            $this->redisModel = new RedisModel();
        }
        return $this->redisModel;
    }
}