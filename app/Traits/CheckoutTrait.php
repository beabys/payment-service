<?php

namespace App\Traits;

use App\Components\Payments\Payment;

/**
 * Class CheckoutTrait
 * @package App\Traits
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
trait CheckoutTrait
{
    protected $paymentComponent;

    /**
     * @return Payment
     */
    public function getPayment()
    {
        if (!$this->paymentComponent instanceof Payment) {
            $this->paymentComponent = new Payment();
        }
        return $this->paymentComponent;
    }
}