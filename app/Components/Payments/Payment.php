<?php

namespace App\Components\Payments;

use App\Models\Payments;
use App\Providers\ExchangeRateServiceProvider;
use App\Providers\PaymentServiceProvider;
use App\Traits\PaymentTrait;
use Exception;
use ExchangeRate\Success as SuccessExchange;
use PaymentEngine\Messages\Success;
use PaymentEngine\Providers\BraintreeProvider;
use PaymentEngine\Providers\PaypalProvider;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class Payment
{
    use PaymentTrait;

    const VISA = 'VISA';
    const MASTERCARD = 'Mastercard';
    const AMEX = 'AMEX';
    const DISCOVER = 'Discover';
    const DINERS = 'Diners';
    const INVALID = 'INVALID';
    const HKD = 'HKD';
    const USD = 'USD';
    const AUD = 'AUD';
    const EUR = 'EUR';
    const JPY = 'JPY';
    const CNY = 'CNY';
    const PAYPAL = 'paypal';
    const BRAINTREE = 'braintree';

    protected $cardType;
    protected $currency;
    protected $parameters;
    protected $paymentMethodName;

    /**
     * @param $parameters
     */
    private function setData($parameters)
    {
        $this->setParameters($parameters);
        $this->setCurrency($parameters['currency']);
        $this->setCardType($this->getCardTypeByNumber($parameters['credit_number']));
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function selectPaymentMethod()
    {
        switch (true) {
            case (self::AMEX == $this->getCardType() && self::USD == $this->getCurrency()):
                $this->setPaymentMethodName(self::PAYPAL);
                break;
            case (self::AMEX != $this->getCardType() &&
                (
                    self::USD == $this->getCurrency() ||
                    self::EUR == $this->getCurrency() ||
                    self::AUD == $this->getCurrency()
                )):
                $this->setPaymentMethodName(self::PAYPAL);
                break;
            case (self::AMEX == $this->getCardType() && self::USD != $this->getCurrency()):
                throw new Exception(sprintf('%s is possible to use only for USD', self::AMEX));
                break;
            default:
                $this->setPaymentMethodName(self::BRAINTREE);
                break;
        }
        return;
    }

    /**
     * Set Price for exchange Rate
     */
    protected function exchangeRate()
    {
        if (self::PAYPAL != $this->getPaymentMethodName()) {
            $data = $this->getParameters();
            $exchangeRate = app(ExchangeRateServiceProvider::BUILDER);
            $currencyBase = config('payments.' . $this->getPaymentMethodName() . '.currency_iso');
            if ($currencyBase != $this->getCurrency()) {
                $exchange = $exchangeRate->build($this->getCurrency());
                if ($exchange instanceof SuccessExchange) {
                    $result = $exchange->getResult();
                    $data['currency_base'] = $currencyBase;
                    $data['price_in_' . $currencyBase] = round($result[$currencyBase] * $data['price'], 2);
                    $this->setParameters($data);
                }
            }
        }
        return;
    }

    /**
     * @param $card
     * @return string
     */
    protected function getCardTypeByNumber($card)
    {
        $card = implode('', explode('-', $card));
        switch(true) {
            //VISA
            case preg_match("/^([4]{1})([0-9]{12,15})$/", $card):
                return self::VISA;
                break;
            //MASTERCARD
            case preg_match("/^([51|52|53|54|55]{2})([0-9]{14})$/", $card):
                return self::MASTERCARD;
                break;
            //AMEX
            case preg_match("/^([34|37]{2})([0-9]{13})$/", $card):
                return self::AMEX;
                break;
            //DISCOVER
            case preg_match("/^([6011]{4})([0-9]{12})$/", $card):
                return self::DISCOVER;
                break;
            //DINERS
            case preg_match("/^([30|36|38]{2})([0-9]{12})$/", $card):
                return self::DINERS;
                break;
            //DEFAULT
            default:
                return self::INVALID;
                break;
        }
    }

    /**
     * @param $paymentName
     * @return Exception|PaypalProvider|BraintreeProvider
     */
    protected function setProvider($paymentName)
    {
        $provider = app(PaymentServiceProvider::BUILDER);
        return $provider->build($paymentName, config('payments.'. $paymentName));
    }

    /**
     * @param Success $payment
     * @return array
     */
    protected function responseToCache(Success $payment)
    {
        $model = $this->getModel();
        return $model->saveId($payment, $this->getParameters());
    }

    /**
     * Add Extra parameters to data for Request
     */
    protected function addExtraParameters()
    {
        $parameters = $this->getParameters();
        //Add more parameters if needed
        $parameters['payment_method_name'] = $this->getPaymentMethodName();
        $parameters['card_type'] = $this->getCardType();
        //set again the parameters
        $this->setParameters($parameters);
    }

    /**
     * @param $response
     */
    protected function saveResponse($response)
    {
        //save to cache
        $toSave = $this->responseToCache($response);
        //save in database
        $paymentModel = new Payments();
        $paymentModel->savePayment($toSave);
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param $cardType
     * @return $this
     * @throws Exception
     */
    public function setCardType($cardType)
    {
        if (self::INVALID == $cardType) {
            throw new Exception('Invalid Card');
        }
        $this->cardType = $cardType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param $parameters
     * @return $this
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodName()
    {
        return $this->paymentMethodName;
    }

    /**
     * @param $paymentMethodName
     * @return $this
     */
    public function setPaymentMethodName($paymentMethodName)
    {
        $this->paymentMethodName = $paymentMethodName;

        return $this;
    }

    /**
     * @param $parameters
     * @return \PaymentEngine\Messages\Error|Success
     */
    public function charge($parameters)
    {
        $this->setData($parameters);
        $this->selectPaymentMethod();
        $this->exchangeRate();
        $this->addExtraParameters();
        //Send Payment to Provider
        $paymentResponse =
            $this->setProvider($this->getPaymentMethodName())
                ->charge($this->getParameters())
        ;
        if ($paymentResponse instanceof Success) {
           $this->saveResponse($paymentResponse);
        }
        return $paymentResponse;
    }
}
