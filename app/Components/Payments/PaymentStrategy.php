<?php

namespace App\Components\Payments;

use PaymentEngine\PaymentStrategy as ParentStrategy;
use PaymentEngine\Providers\BraintreeProvider;
use PaymentEngine\Providers\PaypalProvider;
use Exception;

/**
 * Class PaymentStrategy
 * @package App\Components\Payments
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaymentStrategy extends ParentStrategy
{

    const BRAINTREE = "braintree";
    const PAYPAL = 'paypal';

    /**
     * @param $paymentMethod
     * @param null $params
     * @return BraintreeProvider|PaypalProvider
     * @throws Exception
     */
    public function build($paymentMethod, $params = null)
    {
        switch ($paymentMethod) {
            case self::BRAINTREE :
                return new BraintreeProvider($params);
                break;
            case self::PAYPAL :
                return new PaypalProvider($params);
                break;
            //Add Nes Payment Methods
            default :
                throw new Exception(sprintf("payment Method %s not defined", $paymentMethod));
                break;
        }
    }

}
