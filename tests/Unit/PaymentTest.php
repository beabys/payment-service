<?php

namespace Tests\Unit;

use App\Components\Payments\Payment;
use App\Components\Payments\PaymentStrategy;
use App\Providers\PaymentServiceProvider;
use PaymentEngine\Messages\Error;
use PaymentEngine\Messages\Success;
use Tests\TestCase;
use ReflectionClass;

class PaymentTest extends TestCase
{

    /**
     * @param $object
     * @param $method
     * @param $args
     * @return mixed
     */
    protected function invokeProtectedMethods($object, $method, $args = null) {
        $class = new ReflectionClass(get_class($object));
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        if (is_null($args)) {
            return $method->invoke($object);
        }
        return $method->invoke($object, $args);
    }

    /**
     * Test Creation of instance for paymentStartegy
     */
    public function testIsInstanceOfPaymentStrategy()
    {
        $payment = $provider = app(PaymentServiceProvider::BUILDER);
        $this->assertInstanceOf('App\Components\Payments\PaymentStrategy', $payment, 'Instance of PaymentStrategy');
    }

    /**
     * TEST EXCEPTION CREATING INVALID PROVIDER
     */
    public function testNotExistProvider()
    {
        $this->expectException('Exception');
        $payment = $provider = app(PaymentServiceProvider::BUILDER);
        $payment->build(Mocks::NOT_EXIST);
    }

    /**
     * INSTANCE OF PAYPAL
     */
    public function testPaypalInstance()
    {
        $payment = $provider = app(PaymentServiceProvider::BUILDER);
        $provider = $payment->build(PaymentStrategy::PAYPAL, Mocks::$parametersConstructor);
        $this->assertInstanceOf('PaymentEngine\Providers\PaypalProvider', $provider, 'Instance of Paylpal');
    }

    /**
     * INSTANCE OF BRAINTREE
     */
    public function testBraintreeInstance()
    {
        $payment = $provider = app(PaymentServiceProvider::BUILDER);
        $provider = $payment->build(PaymentStrategy::BRAINTREE, Mocks::$parametersConstructor);
        $this->assertInstanceOf('PaymentEngine\Providers\BraintreeProvider', $provider, 'Instance of Braintree');
    }

    /**
     * INVALID CREDIT CARD TEST
     */
    public function testCreditCardInvalid()
    {
        $payment = new Payment();
        $testCard = $this->invokeProtectedMethods($payment, 'getCardTypeByNumber', Mocks::INVALID);
        $this->assertTrue(Payment::INVALID == $testCard, 'invalid card' );
    }

    /**
     * VALID CREDIT CARD TEST
     */
    public function testCreditCardValid()
    {
        $payment = new Payment();
        $testCard = $this->invokeProtectedMethods($payment, 'getCardTypeByNumber', Mocks::VISA);
        $this->assertTrue(Payment::VISA == $testCard, 'cards are teh same' );
    }

    /**
     * TEST CONDITION FOR AMEX
     */
    public function testProviderSelector()
    {
        $payment = new Payment();
        $payment
            ->setCardType(Payment::AMEX)
            ->setCurrency('USD');
        $this->invokeProtectedMethods($payment, 'selectPaymentMethod');
        $this->assertTrue($payment->getPaymentMethodName() == Payment::PAYPAL, 'cards are teh same' );
    }

    /**
     * TEST INVALID CONDITION FOR AMEX
     */
    public function testProviderSelectorErrorForAmex()
    {
        $this->expectException('Exception');
        $payment = new Payment();
        $payment
            ->setCardType(Payment::AMEX)
            ->setCurrency('HKD');
        $this->invokeProtectedMethods($payment, 'selectPaymentMethod');
    }

    /**
     * TEST PROVIDER RESPONSE SUCCESS RESPONSE
     */
    public function testProcessSuccess()
    {
        //Mock Success
        $success = new Success();
        $success->setCustomerId('test')
                ->setPaymentResult(Mocks::$successResultPaypal)
        ;

        //Mock Provider
        $provider = $this->getMockBuilder('PaymentEngine\Providers\PaypalProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $provider->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($success));

        //Mock Payment Methods
        $payment = $this->getMockBuilder('App\Components\Payments\Payment')
            ->setMethods(['responseToCache', 'setProvider', 'saveResponse'])
            ->getMock();
        $payment
            ->method('responseToCache')
            ->with($success)
            ->willReturn('')
        ;
        $payment
            ->method('setProvider')
            ->willReturn($provider)
        ;
        $payment
            ->method('saveResponse')
            ->with($success)
            ->willReturn('')
        ;
        //$paymentResponse = $this->invokeProtectedMethods($payment, 'charge');
        $paymentResponse = $payment->charge(Mocks::$parametersCharge);
        $this->assertInstanceOf('PaymentEngine\Messages\Success', $paymentResponse);
    }
    /**
     * TEST PROVIDER RESPONSE ERROR RESPONSE
     */
    public function testProcessError()
    {
        //Mock Success
        $error = new Error('invalid');
        $error
            ->setCustomerId('test')
            ->setPaymentResult(Mocks::$successResultPaypal)
        ;

        //Mock Provider
        $providerPaypal = $this->getMockBuilder('PaymentEngine\Providers\PaypalProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $providerPaypal->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($error));
        $providerBraintree = $this->getMockBuilder('PaymentEngine\Providers\BraintreeProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $providerBraintree->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($error));
        //Mock Payment Methods
        $payment = $this->getMockBuilder('App\Components\Payments\Payment')
            ->setMethods(['setProvider'])
            ->getMock();
        $payment
            ->method('setProvider')
            ->willReturn($providerPaypal)
        ;
        $paymentResponse = $payment->charge(Mocks::$parametersCharge);
        $this->assertInstanceOf('PaymentEngine\Messages\Error', $paymentResponse);
    }
}
