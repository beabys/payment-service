<?php
/**
 * Created by PhpStorm.
 * User: beabys
 * Date: 17/06/17
 * Time: 15:19
 */

namespace Tests\Unit;


class Mocks
{
    const NOT_EXIST = 'noPyament';
    const VISA = '4111111111111111';
    const MASTERCARD = '5105105105105100';
    const AMEX = '346815174062418';
    const DISCOVER = '6011164886394905';
    const DINERS = '38333767644652';
    const INVALID = '9999999999999999';

    static $parametersConstructor = [
        'environment' => 'sandbox',
        'client_id' => 'test',
        'secret' => 'test',
        'merchant_id' => 'test',
        'public_key' => 'test',
        'private_key' => 'test',
        'currency_iso' => 'USD'

    ];

    static $parametersCharge = [
        'firstname' => 'test',
        'lastname' => 'test',
        'email' => 'test@test.com',
        'phone' => '1234-1234',
        'price' => '12',
        'currency' => 'USD',
        'customer_id' => 'testttt',
        'holder_name' => 'test',
        'credit_number' => '4111111111111111',
        'cvv' => '123',
        'payment_method_name' => 'paypal',
        'card_type' => 'VISA'
    ];

    static $successResultPaypal = [
        'id' => "PAY-TEEEEEST",
        'create_time' => "2017-06-14T10:59:15Z",
        'update_time' => "2017-06-14T10:59:19Z",
        'state' => "approved",
        'intent' => "sale",
    ];

    static $Braintreesale = [
        'firstName' => 'test',
        'lastName'  => 'test',
        'email'     => 'test@test.com',
        'creditCard' => [
            'number'          => 4111111111111111,
            'cardholderName'  => 'test test',
            'expirationMonth' => 12,
            'expirationYear'  => 21,
            'cvv'             => 123,
        ]
    ];

    static $braintreCharge = [
        'firstname' => "test1",
        'lastname' => "test1",
        'email' =>  "test1@test1.com",
        'phone' => "1234-1234",
        'price' => "13",
        'currency' => "HKD",
        'customer_id' => 123,
        'holder_name' => "test test",
        'credit_number' =>  "4444",
        'credit_expiration' => "12/21",
        'currency_base' => "USD",
        'price_in_USD' => 1.67,
        'payment_method_name' => "braintree",
        'card_type' => "Mastercard",
    ];
}