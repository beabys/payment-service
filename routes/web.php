<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//routes for Home
Route::get('payment',[
    'as' => 'payment',
    'uses' => 'HomeController@create'
]);
Route::post('payment',[
    'as' => 'payment_store',
    'uses' => 'HomeController@store'
]);
//routes for Checkout
Route::get('checkout',[
    'as' => 'checkout',
    'uses' => 'CheckoutController@create'
]);
Route::post('checkout',[
    'as' => 'checkout_process',
    'uses' => 'CheckoutController@proceed'
]);
Route::get('success',[
    'as' => 'success',
    'uses' => 'CheckoutController@success'
]);
Route::post('checking',[
    'as' => 'checking_process',
    'uses' => 'CheckingFormController@getData'
]);
Route::get('checking',[
    'as' => 'checking',
    'uses' => 'CheckingFormController@create'
]);
