Payment Service
=============

## Prerequisites
* [Composer](https://getcomposer.org/)
* [Redis](https://redis.io/)
* [MySQL](https://www.mysql.com/) or [MariaDB](https://mariadb.org/)

## Payment Services Providers
* [Paypal](https://www.paypal.com/)
* [Braintree](https://www.braintreepayments.com/‎)


# Installing
### Set configurations

copy the .env.example to .env and fill the variables required for configuration

    cp .env.example .env

### Configuration Variables

#### Redis

    REDIS_HOST=
    REDIS_PASSWORD=
    REDIS_PORT=
    
| Parameter | Default Values |
| --- | --- |
| REDIS_HOST | 127.0.0.1 |
| REDIS_PASSWORD | null |
| REDIS_PORT | 6379 |


#### Paypal

    PAYPAL_ENVIRONMENT=
    PAYPAL_CLIENT_ID=
    PAYPAL_SECRET=

| Parameter | Description |
| --- | --- |
| PAYPAL_ENVIRONMENT | (default)sandbox or live |
| PAYPAL_CLIENT_ID | Provided by Paypal |
| PAYPAL_SECRET | Provided by Paypal |

#### Braintree

        BRAINTREE_ENVIRONMENT=
        BRAINTREE_MERCHANT_ID=
        BRAINTREE_PUBLIC_KEY=
        BRAINTREE_PRIVATE_KEY=

| Parameter | Description |
| --- | --- |
| BRAINTREE_ENVIRONMENT | (default)sandbox or live |
| BRAINTREE_MERCHANT_ID | Provided by Braintree |
| BRAINTREE_PUBLIC_KEY | Provided by Braintree |
| BRAINTREE_PRIVATE_KEY | Provided by Braintree |


### Install Dependencies

    composer install
    
### Generate Application Key
    
    php artisan key:generate
    
### Run Migrations
    
    php artisan migrate
    
### Clear cache
    
    php artisan cache:clear

### Start Server

    php artisan serve

if you want to use a different port use the follow

    php artisan serve --port[port]


### Routes

##### Start Payment

    http://localhost:[port]/payment

##### Check Payment
    
    http://localhost:[port]/checking
  
### Braintree test cards

* [test cards](https://developers.braintreepayments.com/reference/general/testing/php)

### Paypal Test Cards

* [create test cards](https://www.paypal-knowledge.com/infocenter/index?page=content&widgetview=true&id=FAQ1413)

### Add New Payment Methods

To implement a new payment method, add the new instance of the payment method in the payment strategy

    App\Components\Payments\PaymentStrategy
    
and set the configuration in the .env file and in config\payments.php

## Author

This Payment Service was created by Alfonso Rodriguez ([@beabys](http://twitter.com/beabys)).

